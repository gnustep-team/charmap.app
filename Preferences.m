// Preferences.m - Charmap Preferences implementation
// Copyright (C) 2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _PREFERENCES_H_
#include "Preferences.h"
#endif

@implementation Preferences : NSObject
- (void) dealloc
{
  RELEASE(cjkSwitch);
  RELEASE(preferencesPanel);
  RELEASE(cjkEnabled);
  [super dealloc];
}

- (id) init
{
  cjkEnabled = [[NSUserDefaults standardUserDefaults]
		 objectForKey: @"cjkEnabled"];
  if (!cjkEnabled)
    {
      cjkEnabled = [NSNumber numberWithBool: NO];
      [[NSUserDefaults standardUserDefaults] setObject: cjkEnabled
					     forKey: @"cjkEnabled"];
    }
  return self;
}

- (void) awakeFromNib
{
  [cjkSwitch setState: [self getCJK]];
  [preferencesPanel makeKeyAndOrderFront: self];
}

- (BOOL) getCJK
{
  return [cjkEnabled boolValue];
}

- (void) toggleCJK: (id) sender
{
  if ([cjkSwitch state] == NSOnState)
      cjkEnabled = [NSNumber numberWithBool: YES];
  if ([cjkSwitch state] == NSOffState) 
      cjkEnabled = [NSNumber numberWithBool: NO];
  [[NSUserDefaults standardUserDefaults] setObject: cjkEnabled
					 forKey: @"cjkEnabled"];
  [[NSUserDefaults standardUserDefaults] synchronize];
  [[NSApp delegate] toggleCJK: cjkEnabled];
}  

@end

