// Charmap.h - Charmap general header file
// Copyright (C) 2003,2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CHARMAP_H_
#define _CHARMAP_H_

#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include <AppKit/NSApplication.h>
#include <AppKit/NSNibLoading.h>

#ifndef GNUSTEP
#define RELEASE(object)          [object release]
#define ASSIGN(object,value)     ({\
     id __value = (id)(value); \
     id __object = (id)(object); \
     if (__value != __object) \
       { \
         if (__value != nil) \
           { \
             [__value retain]; \
           } \
         object = __value; \
         if (__object != nil) \
           { \
             [__object release]; \
           } \
       } \
   })
#endif /* __GNUSTEP__ */

#endif /* _CHARMAP_H_ */
