// Preferences.h - Charmap Preferences interface
// Copyright (C) 2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CHARMAP_H_
#include "Charmap.h"
#endif

#ifndef _PREFERENCES_H_
#define _PREFERENCES_H_

@interface Preferences : NSObject
{
  NSButton *cjkSwitch;
  NSNumber *cjkEnabled;
  NSWindow *preferencesPanel;
}
- (BOOL) getCJK;
- (void) toggleCJK: (id) sender;
@end

#endif /* _PREFERENCES_H_ */
