0.1 About Charmap
=================

Charmap is a powerful character map. It is developed using the Openstep
standard, allowing compilation with the GNUstep development environment
(`http://www.gnustep.org') and soon on Mac OSX.  For installation, read
INSTALL. Details follow.

Charmap offers font selection, allowing one to easily see all the
glyphs which a particular font offers.

   Perhaps Charmap's biggest advantage is its support for displaying
Unicode information.

0.2 Unicode support
===================

Charmap can display all Unicode characters up to code point 0xFFFF.
Support for all of the Unicode 4.0 standard will come shortly.

   Charmap is different than most character maps in that it uses the
files which document the Unicode standard to display a wealth of
information to the user. By inspecting a character the user can view
the character's name, UTF-8, octal, and decimal representation.

0.3 CJK support
===============

If CJK support is installed, viewing a CJK character (a character
belonging to the Chinese, Japanese, or Korean languages) will display
the character's meaning. For instructions on viewing CJK information,
see INSTALL.

   Christopher Culver <crculver@users.sourceforge.net>

