// Controller.h - Charmap application delegate interface
// Copyright (C) 2003,2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CHARMAP_H_
#include "Charmap.h"
#endif

#ifndef _CHARINSPECTOR_H_
#include "CharInspector.h"
#endif

#ifndef _CJKINSPECTOR_H_
#include "CJKInspector.h"
#endif

#ifndef _PREFERENCES_H_
#include "Preferences.h"
#endif

#ifndef _UNICODEDATA_H_
#include "UnicodeData.h"
#endif

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

@interface Controller : NSObject
{
  NSArray *blockNames;
  NSBrowser *blocksBrowser;
  NSButton *copyButton;
  NSMutableDictionary *cellDict;
  NSMutableDictionary *largeDict;
  NSMatrix *charmapMatrix;
  NSMenu *mainMenu;
  NSMenuItem *cjkMenuItem;
  NSPanel *preferencesPanel;
  NSScrollView *charmapScroll;
  NSTextField *display;
  NSTextView *cjkChar;
  NSTextView *cjkDefinition;
  NSTextFieldCell *charmapCell;

  CharInspector *charInspector;
  CJKInspector *cjkInspector;
  Preferences *preferences;
  UnicodeData *unicodeData;

  BOOL cjkInspectorOpen;
  BOOL charInspectorOpen;
  BOOL showCJK;
  BOOL showUnicode;
}
- (void)awakeFromNib;
- (void)generateCharmap;
- (void)updateInspectors: (id)sender;
- (void)showPreferences: (id)sender;
- (void)showCJKInspector: (id)sender;
- (void)showCharInspector: (id)sender;
- (void)changeFont: (id)fontManager;
- (void)toggleCJK: (NSNumber *)cjkState;
- (void)browser: (NSBrowser *)sender
createRowsForColumn: (int)column
      inMatrix: (NSMatrix *)matrix;
- (void)adjustCharmapMatrixWidth;
@end

#endif /* _CONTROLLER_H_ */
