// CharInspector.m - Charmap Character Inspector implementation
// Copyright (C) 2003-2005 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CHARINSPECTOR_H_
#include "CharInspector.h"
#endif

@implementation CharInspector : NSObject
- (void) dealloc
{
  RELEASE(charDecimalInfo);
  RELEASE(charOctalInfo);
  RELEASE(charUTF8Info);
  RELEASE(charAliasInfo);
  RELEASE(charCategoryInfo);
  RELEASE(charDecompInfo);
  RELEASE(charNameInfo);
  RELEASE(inspectorChar);
  RELEASE(assignedView);
  RELEASE(unassignedText);
  RELEASE(unassignedView);
  RELEASE(inspectorView);
  RELEASE(inspectorPanel);
  RELEASE(largeDict);
  RELEASE(smallDict);
  RELEASE(unassignedDict);
  [super dealloc];
}

- (id) init
{
  NSMutableParagraphStyle * paragraphStyle = [NSMutableParagraphStyle
					       defaultParagraphStyle];
  [paragraphStyle setAlignment: NSCenterTextAlignment];

  ASSIGN(largeDict, ([NSDictionary dictionaryWithObjectsAndKeys:
					    [NSFont systemFontOfSize: 48],
					  NSFontAttributeName,
					  [NSColor blueColor],
					  NSForegroundColorAttributeName,
					  paragraphStyle,
					  NSParagraphStyleAttributeName,
					  nil]));

  ASSIGN(smallDict, ([NSDictionary dictionaryWithObjectsAndKeys:
					    [NSFont systemFontOfSize: 10],
					  NSFontAttributeName,
					  [NSColor redColor],
					  NSForegroundColorAttributeName,
					  paragraphStyle,
					  NSParagraphStyleAttributeName,
					  nil]));

  ASSIGN(unassignedDict, ([NSDictionary dictionaryWithObjectsAndKeys:
					    [NSFont systemFontOfSize: 24],
					  NSFontAttributeName,
					  [NSColor redColor],
					  NSForegroundColorAttributeName,
					  paragraphStyle,
					  NSParagraphStyleAttributeName,
					  nil]));

  return self;
}

- (void) awakeFromNib
{
  [inspectorChar setAlignment: NSLeftTextAlignment];
  [inspectorChar setDrawsBackground: NO];
  [inspectorChar setEditable: NO];
  [inspectorChar setRichText: YES];
  [inspectorChar setSelectable: NO];
  [inspectorChar setUsesFontPanel: NO];
  [charAliasInfo setDrawsBackground: NO];
  [charAliasInfo setEditable: NO];
  [charAliasInfo setSelectable: NO];
  [[charAliasInfo textContainer] setContainerSize:
				   NSMakeSize([charAliasInfo frame].size.width,
					      50)];
  [[charAliasInfo textContainer] setWidthTracksTextView: YES];
  [charDecompInfo setDrawsBackground: NO];
  [charDecompInfo setEditable: NO];
  [charDecompInfo setSelectable: NO];
  [[charDecompInfo textContainer] setWidthTracksTextView: YES];
  [[charDecompInfo textStorage] setAttributedString: nil];
  [charNameInfo setDrawsBackground: NO];
  [charNameInfo setEditable: NO];
  [charNameInfo setSelectable: NO];
  [[charNameInfo textContainer] setWidthTracksTextView: YES];
  [[charNameInfo textStorage] setAttributedString:
  			[[NSAttributedString alloc]
  			  initWithString: @"No character selected"]];

  /* Create the view displayed when unassigned position is selected. */
  unassignedView = [[NSBox alloc] initWithFrame: NSMakeRect(0, 89, 330, 224)];
  [unassignedView setBorderType: NSNoBorder];
  [unassignedView setTitlePosition: NSNoTitle];
  unassignedText = [[NSTextView alloc]
		     initWithFrame: NSMakeRect(0, -25, 326, 100)];
  [[charAliasInfo textContainer] setWidthTracksTextView: YES];
  [[unassignedText textStorage]
    setAttributedString: [[NSAttributedString alloc]
			   initWithString: @"This position is not yet assigned."
			   attributes: unassignedDict]];
  [unassignedText setEditable: NO];
  [unassignedText setDrawsBackground: NO];
  [unassignedView addSubview: unassignedText];

  [inspectorPanel makeKeyAndOrderFront: self];
} 

- (void) updateWithPosition: (unichar) position
		Dictionary: (NSDictionary *) charDict
{
  NSAttributedString *charAliasAttrString = nil;
  NSAttributedString *charNameAttrString = nil;
  NSMutableString *charDecompString = nil;
  NSMutableString *tempString;
  NSString *character;
  NSString *charNameString = nil;
  NSString *name = nil;
  const unsigned char * cs;

  if (position < 55296 || position > 56316)
    character = [[NSString alloc] initWithCharacters: &position length: 1];
  else
    character = [[NSString alloc] init];

  name = [charDict objectForKey: @"Name"];
  
  // if ((name != nil && [name isEqual: @"<not assigned>"] == NO)
  if ([name isEqual: @"<not assigned>"] == NO)
    {  
      if ([inspectorView ancestorSharedWithView: unassignedView])
	{
	  RETAIN(unassignedView);
	  [inspectorView replaceSubview: unassignedView
			 with: assignedView];
	}
      
      [[inspectorChar textStorage] setAttributedString: nil];
      [[charAliasInfo textStorage] setAttributedString: nil];
      /* Show name. */
      charNameString = [[NSString alloc] initWithFormat: @"U+%004X %s",
					 position, [name cString]];
      charNameAttrString = [[NSAttributedString alloc]
			     initWithString: charNameString];
      if (charNameAttrString)
	[[charNameInfo textStorage] setAttributedString: charNameAttrString];
      
      /* Show alias. */
      if ([charDict objectForKey: @"Unicode1.0Name"])
	{
	  charAliasAttrString = [[NSAttributedString alloc]
				  initWithString:
				    [charDict objectForKey:
						@"Unicode1.0Name"]];
	  [[charAliasInfo textStorage]
	    setAttributedString: charAliasAttrString];
	}
      
      /* Show general Unicode category. */
      if ([charDict objectForKey: @"GeneralCategory"])
	[charCategoryInfo setStringValue: [charDict objectForKey:
						      @"FullCategory"]];
      else
	[charCategoryInfo setStringValue: nil];
      [charCategoryInfo setNeedsDisplay: YES];
      
      /* Show canonical decomposition. */
      [[charDecompInfo textStorage] setAttributedString: nil];
      charDecompString = [charDict objectForKey: @"FullDecomposition"];
      [[charDecompInfo textStorage] setAttributedString:
				      [[NSAttributedString alloc]
					initWithString: charDecompString]];
      
      /* Show character "magnified" if displayable */
      if ([[charDict objectForKey: @"Name"] isEqual: @"<control>"] == NO
	  && (position < 55296 || position > 57339))
	{
	  [[inspectorChar textStorage] setAttributedString:
					 [[NSAttributedString alloc]
					   initWithString: character
					   attributes: largeDict]];
	}
      else /* Otherwise say that it's not displayable. */
	{
	  [[inspectorChar textStorage] setAttributedString:
					 [[NSAttributedString alloc]
					   initWithString: @"\nNot a printable character"
					   attributes: smallDict]];
	}
      RELEASE(charAliasAttrString);
      RELEASE(charNameAttrString);
    }
  else /* Show unassigned view */
    {
      if ([inspectorView ancestorSharedWithView: assignedView])
	{
	  RETAIN(assignedView);
	  [inspectorView replaceSubview: assignedView
			 with: unassignedView];
	}
    }
  /* Set various representations. */
  [charDecimalInfo setStringValue: [NSString stringWithFormat: @"&#%d;",
					     position]];
  
  tempString = [[NSMutableString alloc] init];
  for (cs = [character UTF8String]; *cs != '\0'; cs++)
    [tempString appendString: [NSString stringWithFormat: @"0x%02X ", *cs]];
  [charUTF8Info setStringValue: tempString];
  RELEASE(tempString);
  
  tempString = [[NSMutableString alloc] init];
  for (cs = [character UTF8String]; *cs != '\0'; cs++)
    [tempString appendString: [NSString stringWithFormat: @"\\%02o", *cs]];
  [charOctalInfo setStringValue: tempString];
  RELEASE(character);       
}

- (void) setNewFont: (NSFont *) newLargeFont
	 Dictionary: (NSDictionary *) newLargeDict
{
  [inspectorChar setFont: newLargeFont];
  largeDict = newLargeDict;
}

@end
