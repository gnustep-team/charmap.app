// CJKInspector.m - Charmap CJK Inspector implementation
// Copyright (C) 2003,2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CJKINSPECTOR_H_
#include "CJKInspector.h"
#endif

@implementation CJKInspector : NSObject
- (void) dealloc
{
  RELEASE(cjkCantonese);
  RELEASE(cjkDefinition);
  RELEASE(cjkKorean);
  RELEASE(cjkKun);
  RELEASE(cjkMandarin);
  RELEASE(cjkOn);
  RELEASE(cjkStrokeCount);
  RELEASE(cjkView);
  RELEASE(nonCJKText);
  RELEASE(nonCJKDict);
  RELEASE(nonCJKView);
  RELEASE(inspectorPanel);
  [super dealloc];
}

- (id) init
{
  NSMutableParagraphStyle * paragraphStyle = [NSMutableParagraphStyle
					       defaultParagraphStyle];
  [paragraphStyle setAlignment: NSCenterTextAlignment];

  ASSIGN(nonCJKDict, ([NSDictionary dictionaryWithObjectsAndKeys:
					    [NSFont systemFontOfSize: 24],
					  NSFontAttributeName,
					  [NSColor redColor],
					  NSForegroundColorAttributeName,
					  paragraphStyle,
					  NSParagraphStyleAttributeName,
					  nil]));

  return self;
}

- (void) awakeFromNib
{
  [cjkCantonese setStringValue: nil];
  [cjkDefinition setDrawsBackground: NO];
  [cjkDefinition setEditable: NO];
  [cjkDefinition setSelectable: NO];
  [[cjkDefinition textContainer]
   setContainerSize: NSMakeSize([cjkDefinition frame].size.width,
  				 50)];
  [[cjkDefinition textContainer] setWidthTracksTextView: YES];
  [cjkKorean setStringValue: nil];
  [cjkKun setStringValue: nil];
  [cjkMandarin setStringValue: nil];
  [cjkOn setStringValue: nil];
  [cjkStrokeCount setStringValue: nil];
  [inspectorPanel makeKeyAndOrderFront: self];

  /* Create the view displayed when non-CJK position is selected. */
  nonCJKView = [[NSBox alloc] initWithFrame: NSMakeRect(0, 89, 345, 231)];
  [nonCJKView setBorderType: NSNoBorder];
  [nonCJKView setTitlePosition: NSNoTitle];
  nonCJKText = [[NSTextView alloc]
		     initWithFrame: NSMakeRect(0, -25, 326, 100)];
  [[nonCJKText textStorage]
    setAttributedString: [[NSAttributedString alloc]
			   initWithString: @"No CJK character selected."
			   attributes: nonCJKDict]];
  [nonCJKText setEditable: NO];
  [nonCJKText setDrawsBackground: NO];
  [nonCJKView addSubview: nonCJKText];
} 

- (void) updateWithPosition: (unichar) position
		Dictionary: (NSDictionary *) charDict
{
  NSString *cjkDefString;

  if ([charDict objectForKey: @"isCJK"])
    {
      if ([[inspectorPanel contentView] ancestorSharedWithView: nonCJKView])
	{
	  RETAIN(nonCJKView);
	  [[inspectorPanel contentView] replaceSubview: nonCJKView
			 with: cjkView];
	}

      /* Show English definition. */
      if ([charDict objectForKey: @"kDefinition"])
	{
	  [[cjkDefinition textStorage] setAttributedString: nil];
	  cjkDefString = [charDict objectForKey: @"kDefinition"];
	  [[cjkDefinition textStorage] setAttributedString:
					 [[NSAttributedString alloc]
					   initWithString: cjkDefString]];
	}
      else
	[[cjkDefinition textStorage] setAttributedString: nil];
      if ([charDict objectForKey: @"kTotalStrokes"])
	  [cjkStrokeCount setStringValue: [charDict objectForKey:
						      @"kTotalStrokes"]];
      else
	[cjkStrokeCount setStringValue: nil];
      /* Show pronunciations. */
      if ([charDict objectForKey: @"kCantonese"])
	[cjkCantonese setStringValue:  [charDict objectForKey: @"kCantonese"]];
      else
	[cjkCantonese setStringValue: nil];
      if ([charDict objectForKey: @"kKorean"])
	[cjkKorean setStringValue:  [charDict objectForKey: @"kKorean"]];
      else
	[cjkKorean setStringValue: nil];
      if ([charDict objectForKey: @"kJapaneseKun"])
	[cjkKun setStringValue:  [charDict objectForKey: @"kJapaneseKun"]];
      else
	[cjkKun setStringValue: nil];
      if ([charDict objectForKey: @"kMandarin"])
	[cjkMandarin setStringValue:  [charDict objectForKey: @"kMandarin"]];
      else
	[cjkMandarin setStringValue: nil];
      if ([charDict objectForKey: @"kJapaneseOn"])
	[cjkOn setStringValue:  [charDict objectForKey: @"kJapaneseOn"]];
      else
	[cjkOn setStringValue: nil];		  
    }
  else
    {
      if ([[inspectorPanel contentView] ancestorSharedWithView: cjkView])
	{
	  RETAIN(cjkView);
	  [[inspectorPanel contentView] replaceSubview: cjkView
			 with: nonCJKView];
	}
    }
}

- (void) blankInspectorWithMessage: (NSString *) message
{
  [cjkUnicodeName setStringValue: message];
  [[cjkDefinition textStorage] setAttributedString: nil];
}

@end
