// CharInspector.h - Charmap Character Inspector interface
// Copyright (C) 2003,2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CHARMAP_H_
#include "Charmap.h"
#endif

#ifndef _CHARINSPECTOR_H_
#define _CHARINSPECTOR_H_

@interface CharInspector : NSObject
{
  NSBox *inspectorView;
  NSBox *assignedView;
  NSBox *unassignedView;
  NSDictionary *largeDict;
  NSDictionary *smallDict;
  NSDictionary *unassignedDict;
  NSTextField *charCategoryInfo;
  NSTextField *charDecimalInfo;
  NSTextField *charOctalInfo;
  NSTextField *charUTF8Info;
  NSTextView *charAliasInfo;
  NSTextView *charDecompInfo;
  NSTextView *charNameInfo;
  NSTextView *inspectorChar;
  NSTextView *unassignedText;
  NSWindow *inspectorPanel;

  BOOL windowOpen;
}
-(void)updateWithPosition: (unichar) position
	       Dictionary: (NSDictionary *) charDict;
- (void) setNewFont: (NSFont *) newLargeFont
	       Dictionary: (NSDictionary *) newLargeDict;
@end

#endif /* _CHARINSPECTOR_H_ */
