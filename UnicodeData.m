// UnicodeData.m - Charmap application UnicodeData class implementation
// Copyright (C) 2003,2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _UNICODE_DATA_H_
#include <UnicodeData.h>
#endif

@implementation UnicodeData : NSObject
{
}
- (void) dealloc
{
  RELEASE(blockNames);
  RELEASE(categoriesDict);
  if (unihanDict)
    RELEASE(unihanDict);

  [super dealloc];
}
- (id) init
{
  NSString *path;

  // CJK disabled until signal given.
  cjk = NO;

  // Load plist with general Unicode info
  path = [[NSBundle mainBundle] pathForResource: @"UnicodeData"
  				ofType: @"plist"];
  unicodeDict = [[NSDictionary alloc]
		  initWithDictionary:
		    [[NSString stringWithContentsOfFile:
				 path] propertyList]]; 

  blockNames = [[NSArray alloc]
		 initWithObjects: _(@"Basic Latin"),
		 _(@"Latin-1 Supplement"),
		 _(@"Latin Extended-A"),
		 _(@"Latin Extended-B"),
		 _(@"IPA Extensions"),
		 _(@"Spacing Modifier Letters"),
		 _(@"Combining Diacritical Marks"),
		 _(@"Greek and Coptic"),
		 _(@"Cyrillic"),
		 _(@"Cyrillic Supplementary"),
		 _(@"Armenian"),
		 _(@"Hebrew"),
		 _(@"Arabic"),
		 _(@"Syriac"),
		 _(@"Thaana"),
		 _(@"Devanagari"),
		 _(@"Bengali"),
		 _(@"Gurmukhi"),
		 _(@"Gujarati"),
		 _(@"Oriya"),
		 _(@"Tamil"),
		 _(@"Telugu"),
		 _(@"Kannada"),
		 _(@"Malayalam"),
		 _(@"Sinhala"),
		 _(@"Thai"),
		 _(@"Lao"),
		 _(@"Tibetan"),
		 _(@"Myanmar"),
		 _(@"Georgian"),
		 _(@"Hangul Jamo"),
		 _(@"Ethiopic"),
		 _(@"Cherokee"),
		 _(@"Unified Canadian Aboriginal Syllabics"),
		 _(@"Ogham"),
		 _(@"Runic"),
		 _(@"Tagalog"),
		 _(@"Hanunoo"),
		 _(@"Buhid"),
		 _(@"Tagbanwa"),
		 _(@"Khmer"),
		 _(@"Mongolian"),
		 _(@"Limbu"),
		 _(@"Tai Le"),
		 _(@"Khmer Symbols"),
		 _(@"Phonetic Extensions"),
		 _(@"Latin Extended Additional"),
		 _(@"Greek Extended"),
		 _(@"General Punctuation"),
		 _(@"Superscripts and Subscripts"),
		 _(@"Currency Symbols"),
		 _(@"Combining Diacritical Marks for Symbols"),
		 _(@"Letterlike Symbols"),
		 _(@"Number Forms"),
		 _(@"Arrows"),
		 _(@"Mathematical Operators"),
		 _(@"Miscellaneous Technical"),
		 _(@"Control Pictures"),
		 _(@"Optical Character Recognition"),
		 _(@"Enclosed Alphanumerics"),
		 _(@"Box Drawing"),
		 _(@"Block Elements"),
		 _(@"Geometic Shapes"),
		 _(@"Miscellaneous Symbols"),
		 _(@"Dingbats"),
		 _(@"Miscellaneous Mathematical Symbols-A"),
		 _(@"Supplemental Arrows-A"),
		 _(@"Braille Patterns"),
		 _(@"Supplemental Arrows-B"),
		 _(@"Miscellaneous Mathematical Symbols-B"),
		 _(@"Supplemental Mathematical Operators"),
		 _(@"Miscellaneous Symbols and Arrows"),
		 _(@"CJK Radicals Supplement"),
		 _(@"Kangxi Radicals"),
		 _(@"Ideographic Description Characters"),
		 _(@"CJK Symbols and Punctuation"),
		 _(@"Hiragana"),
		 _(@"Katakana"),
		 _(@"Bopomofo"),
		 _(@"Hangul Compatibility Jamo"),
		 _(@"Kanbun"),
		 _(@"Bopomofo Extended"),
		 _(@"Katakana Phonetic Extensions"),
		 _(@"Enclosed CJK Letters and Months"),
		 _(@"CJK Compatibility"),
		 _(@"CJK Unified Ideographs Extensions A"),
		 _(@"Yijing Hexagram Symbols"),
		 _(@"CJK Unified Ideographs"),
		 _(@"Yi Syllables"),
		 _(@"Yi Radicals"),
		 _(@"Hangul Syllables"),
		 _(@"High Surrogates"),
		 _(@"High Private Use Surrogates"),
		 _(@"Low Surrogates"),
		 _(@"Private Use Area"),
		 _(@"CJK Compatibility Ideographs"),
		 _(@"Alphabetic Presentation Forms"),
		 _(@"Arabic Presentation Forms-A"),
		 _(@"Variation Selectors"),
		 _(@"Combining Half Marks"),
		 _(@"CJK Compatibility Forms"),
		 _(@"Small Form Variants"),
		 _(@"Arabic Presentation Forms-B"),
		 _(@"Halfwidth and Fullwidth Forms"),
		 _(@"Specials"),
		 /* Commented out until
		    support of higher
		    portions of Unicode 
		    is implemented. */
// 		 _(@"Linear B Syllabary"),
// 		 _(@"Linear B Ideograms"),
// 		 _(@"Aegean Numbers"),
// 		 _(@"Old Italic"),
// 		 _(@"Gothic"),
// 		 _(@"Ugaritic"),
// 		 _(@"Deseret"),
// 		 _(@"Shavian"),
// 		 _(@"Osmanya"),
// 		 _(@"Cypriot Syllabary"),
// 		 _(@"Byzantine Musical Symbols"),
// 		 _(@"Musical Symbols"),
// 		 _(@"Tai Xuan Jing Symbols"),
// 		 _(@"Mathematical Alphanumeric Symbols"),
// 		 _(@"CJK Unified Ideographs Extensions B"),
// 		 _(@"CJK Compatibility Ideographs Supplement"),
// 		 _(@"Tags"),
// 		 _(@"Variation Selectors Supplement"),
// 		 _(@"Supplementary Private Use Area-A"),
// 		 _(@"Supplementary Private Use Area-B"),
		 nil];
  blockOffsets[0] = 0x0000; // Basic Latin
  blockOffsets[1] = 0x0080; // Latin-1 Supplement
  blockOffsets[2] = 0x0100; // Latin Extended-A
  blockOffsets[3] = 0x0180; // Latin Extended-B
  blockOffsets[4] = 0x0250; // IPA Extensions
  blockOffsets[5] = 0x02B0; // Spacing Modifier Letters
  blockOffsets[6] = 0x0300; // Combining Diacritical Marks
  blockOffsets[7] = 0x0370; // Greek and Coptic
  blockOffsets[8] = 0x0400; // Cyrillic
  blockOffsets[9] = 0x0500; // Cyrillic Supplementary
  blockOffsets[10] = 0x0530; // Armenian
  blockOffsets[11] = 0x0590; // Hebrew
  blockOffsets[12] = 0x0600; // Arabic
  blockOffsets[13] = 0x0700; // Syriac
  blockOffsets[14] = 0x0780; // Thaana
  blockOffsets[15] = 0x0900; // Devanagari
  blockOffsets[16] = 0x0980; // Bengali
  blockOffsets[17] = 0x0A00; // Gurmukhi
  blockOffsets[18] = 0x0A80; // Gujarati
  blockOffsets[19] = 0x0B00; // Oriya
  blockOffsets[20] = 0x0B80; // Tamil
  blockOffsets[21] = 0x0C00; // Telugu
  blockOffsets[22] = 0x0C80; // Kannada
  blockOffsets[23] = 0x0D00; // Malayalam
  blockOffsets[24] = 0x0D80; // Sinhala
  blockOffsets[25] = 0x0E00; // Thai
  blockOffsets[26] = 0x0E80; // Lao
  blockOffsets[27] = 0x0F00; // Tibetan
  blockOffsets[28] = 0x1000; // Myanmar
  blockOffsets[29] = 0x10A0; // Georgian
  blockOffsets[30] = 0x1100; // Hangul Jamo
  blockOffsets[31] = 0x1200; // Ethiopic
  blockOffsets[32] = 0x13A0; // Cherokee
  blockOffsets[33] = 0x1400; // Unified Canadian Aboriginal Syllabics
  blockOffsets[34] = 0x1680; // Ogham
  blockOffsets[35] = 0x16A0; // Runic
  blockOffsets[36] = 0x1700; // Tagalog
  blockOffsets[37] = 0x1720; // Hanunoo
  blockOffsets[38] = 0x1740; // Buhid
  blockOffsets[39] = 0x1760; // Tagbanwa
  blockOffsets[40] = 0x1780; // Khmer
  blockOffsets[41] = 0x1800; // Mongolian
  blockOffsets[42] = 0x1900; // Limbu
  blockOffsets[43] = 0x1950; // Tai Le
  blockOffsets[44] = 0x19E0; // Khmer Symbols
  blockOffsets[45] = 0x1D00; // Phonetic Extensions
  blockOffsets[46] = 0x1E00; // Latin Extended Additional
  blockOffsets[47] = 0x1F00; // Greek Extended
  blockOffsets[48] = 0x2000; // General Punctuation
  blockOffsets[49] = 0x2070; // Superscripts and Subscripts
  blockOffsets[50] = 0x20A0; // Currency Symbols
  blockOffsets[51] = 0x20D0; // Combining Diacritical Marks for Symbols
  blockOffsets[52] = 0x2100; // Letterlike Symbols
  blockOffsets[53] = 0x2150; // Number Forms
  blockOffsets[54] = 0x2190; // Arrows
  blockOffsets[55] = 0x2200; // Mathematical Operators
  blockOffsets[56] = 0x2300; // Miscellaneous Technical
  blockOffsets[57] = 0x2400; // Control Pictures
  blockOffsets[58] = 0x2440; // Optical Character Recognitio
  blockOffsets[59] = 0x2460; // Enclosed Alphanumerics
  blockOffsets[60] = 0x2500; // Box Drawing
  blockOffsets[61] = 0x2580; // Block Elements
  blockOffsets[62] = 0x25A0; // Geometric Shapes
  blockOffsets[63] = 0x2600; // Miscellaneous Symbols
  blockOffsets[64] = 0x2700; // Dingbats
  blockOffsets[65] = 0x27C0; // Miscellaneous Mathematical Symbols-A
  blockOffsets[66] = 0x27F0; // Supplemental Arrows-A
  blockOffsets[67] = 0x2800; // Braille Patterns
  blockOffsets[68] = 0x2900; // Supplemental Arrows-B
  blockOffsets[69] = 0x2980; // Miscellaneous Mathematical Symbols-B
  blockOffsets[70] = 0x2A00; // Supplemental Mathematical Operators
  blockOffsets[71] = 0x2B00; // Miscellaneous Symbols and Arrows
  blockOffsets[72] = 0x2E80; // CJK Radicals Supplement
  blockOffsets[73] = 0x2F00; // Kangxi Radicals
  blockOffsets[74] = 0x2FF0; // Ideographic Description Characters
  blockOffsets[75] = 0x3000; // CJK Symbols and Punctuation
  blockOffsets[76] = 0x3040; // Hiragana
  blockOffsets[77] = 0x30A0; // Katakana
  blockOffsets[78] = 0x3100; // Bopomofo
  blockOffsets[79] = 0x3130; // Hangul Compatibility Jamo
  blockOffsets[80] = 0x3190; // Kanbun
  blockOffsets[81] = 0x31A0; // Bopomofo Extended
  blockOffsets[82] = 0x31F0; // Katakana Phonetic Extensions
  blockOffsets[83] = 0x3200; // Enclosed CJK Letters and Months
  blockOffsets[84] = 0x3300; // CJK Compatibility
  blockOffsets[85] = 0x3400; // CJK Unified Ideographs Extension A
  blockOffsets[86] = 0x4DC0; // Yijing Hexagram Symbols
  blockOffsets[87] = 0x4E00; // CJK Unified Ideographs
  blockOffsets[88] = 0xA000; // Yi Syllables
  blockOffsets[89] = 0xA490; // Yi Radicals
  blockOffsets[90] = 0xAC00; // Hangul Syllables
  blockOffsets[91] = 0xD800; // High Surrogates
  blockOffsets[92] = 0xDB80; // High Private Use Surrogates
  blockOffsets[93] = 0xDC00; // Low Surrogates
  blockOffsets[94] = 0xE000; // Private Use Area
  blockOffsets[95] = 0xF900; // CJK Compatibility Ideographs
  blockOffsets[96] = 0xFB00; // Alphabetic Presentation Forms
  blockOffsets[97] = 0xFB50; // Arabic Presentation Forms-A
  blockOffsets[98] = 0xFE00; // Variation Selectors
  blockOffsets[99] = 0xFE20; // Combining Half Marks
  blockOffsets[100] = 0xFE30; // CJK Compatibility Forms
  blockOffsets[101] = 0xFE50; // Small Form Variants
  blockOffsets[102] = 0xFE70; // Arabic Presentation Forms-B
  blockOffsets[103] = 0xFF00; // Halfwidth and Fullwidth Forms
  blockOffsets[104] = 0xFFF0; // Specials
  blockOffsets[105] = 0x10000; // Linear B Syllabary
  blockOffsets[106] = 0x10080; // Linear B Ideograms
  blockOffsets[107] = 0x10100; // Aegean Numbers
  blockOffsets[108] = 0x10300; // Old Italic
  blockOffsets[109] = 0x10330; // Gothic
  blockOffsets[110] = 0x10380; // Ugaritic
  blockOffsets[111] = 0x10400; // Deseret
  blockOffsets[112] = 0x10450; // Shavian
  blockOffsets[113] = 0x10480; // Osmanya
  blockOffsets[114] = 0x10800; // Cypriot Syllabary
  blockOffsets[115] = 0x1D000; // Byzantine Musical Symbols
  blockOffsets[116] = 0x1D100; // Musical Symbols
  blockOffsets[117] = 0x1D300; // Tai Xuan Jing Symbols
  blockOffsets[118] = 0x1D400; // Mathematical Alphanumeric Symbols
  blockOffsets[119] = 0x20000; // CJK Unified Ideographs Extension B
  blockOffsets[120] = 0x2F800; // CJK Compatibility Ideographs Supplement
  blockOffsets[121] = 0xE0000; // Tags
  blockOffsets[122] = 0xE0100; // Variation Selectors Supplement
  blockOffsets[123] = 0xF0000; // Supplementary Private Use Area-A
  blockOffsets[124] = 0x100000; // Supplementary Private Use Area-B
  blockOffsets[125] = (0x10FFFF + 1); // END

  categoriesDict = [[NSDictionary alloc] initWithObjectsAndKeys:
					   _(@"Other, Control"), @"Cc",
					 _(@"Other, Format"), @"Cf",
					 _(@"Other, Not Assigned"), @"Cn",
					 _(@"Other, Private Use"), @"Co",
					 _(@"Other, Surrogate"), @"Cs",
					 _(@"Letter, Lowercase"), @"Ll",
					 _(@"Letter, Modifier"), @"Lm", 
					 _(@"Letter, Other"), @"Lo",
					 _(@"Letter, Titlecase"), @"Lt",
					 _(@"Letter, Uppercase"), @"Lu",
					 _(@" Mark, Spacing Combining"), @"Mc",
					 _(@"Mark, Enclosing"), @"Me",
					 _(@"Mark, Non-Spacing"), @"Mn",
					 _(@"Number, Decimal Digit"), @"Nd",
					 _(@"Number, Letter"), @"Nl",
					 _(@"Number, Other"), @"No",
					 _(@"Punctuation, Connector"), @"Pc",
					 _(@"Punctuation, Dash"), @"Pd", 
					 _(@"Punctuation, Closed"), @"Pe",
					 _(@"Punctuation, Final Quote"), @"Pf",
					 _(@"Punctuation, Initial Quote"), @"Pi",
					 _(@"Punctuation, Other"), @"Po", 
					 _(@"Punctuation, Open"), @"Ps", 
					 _(@"Symbol, Currency"), @"Sc", 
					 _(@"Symbol, Modifier"), @"Sk",
					 _(@"Symbol, Math"), @"Sm", 
					 _(@"Symbol, Other"), @"So",
					 _(@"Zl"), @"Zl",
					 _(@"Separator, Paragraph"), @"Zp",
					 _(@"Separator, Space"), @"Zs", nil];
  

  return self;
}

- (void) enableCJK
{
  NSString *path;

  path = [[NSBundle mainBundle] pathForResource: @"Unihan"
				ofType: @"plist"];

  if (cjk == NO)
    {
      NSLog(@"UnicodeData is loading the CJK plist...");
      unihanDict = [[NSDictionary alloc] initWithDictionary:
					   [[NSString stringWithContentsOfFile:
						       path]
					 propertyList]];
      cjk = YES;
    }
}

- (void) disableCJK
{
  if (cjk == YES)
    {
      NSLog(@"Disabling CJK...");
      if (unihanDict)
	RELEASE(unihanDict);
      cjk = NO;
    }
}

- (NSArray *) getBlockNames
{
  return blockNames;
}

- (int) getBlockSize: (int) unicodeBlock
{
  return ((blockOffsets[unicodeBlock+1]) - blockOffsets[unicodeBlock]);
}

- (long) getBlockStart: (int) unicodeBlock
{
  return (blockOffsets[unicodeBlock]);
}

- (long) getBlockEnd: (int) unicodeBlock
{
  return (blockOffsets[unicodeBlock + 1] - 1);
}

- (NSString *) getNumberString: (int) number
{
  return [[NSString alloc] initWithFormat: @"%0.4X", number];
}

- (NSDictionary *) dictionaryForCharacter: (int) number
{
  NSArray *charInfo;
  NSDictionary *charDict;
  NSMutableDictionary *combinedDict;
  NSString *charName;
  NSString *charCategory;
  NSString *numberString;

  numberString = [self getNumberString: number];
  charInfo = [unicodeDict objectForKey: numberString];

  charName = [self getName: number];

  if ([[charInfo objectAtIndex: 1] length] > 1)
    charCategory = [charInfo objectAtIndex: 1];
  else if ([charName isEqual: _(@"<not assigned>")])
    charCategory = @"Cn";
  else
    charCategory = [self getCategory: number];


  charDict = [NSDictionary dictionaryWithObjectsAndKeys:
			     charName, @"Name",
			   charCategory, @"GeneralCategory",
			   [charInfo objectAtIndex: 1], @"CanonicalCombiningClass",
			   [charInfo objectAtIndex: 2], @"BidirectionalCategory",
			   [charInfo objectAtIndex: 3], @"Decomposition",
			   [charInfo objectAtIndex: 4], @"DecimalDigitValue",
			   [charInfo objectAtIndex: 5], @"DigitValue",
			   [charInfo objectAtIndex: 6], @"NumericValue",
			   [charInfo objectAtIndex: 7], @"Mirrored",
			   [charInfo objectAtIndex: 8], @"Unicode1.0Name",
			   [self getFullDecomposition: [charInfo
							 objectAtIndex: 4]],
			   @"FullDecomposition",
			   [self getFullCategoryName: charCategory],
			   @"FullCategory",
			   nil];
  if ( (cjk == YES) && [unihanDict objectForKey: numberString])
    {
      combinedDict = [[NSMutableDictionary alloc]
		       initWithDictionary:
			 [unihanDict objectForKey: numberString]];
      [combinedDict setObject: @"YES" forKey: @"isCJK"];
      [combinedDict addEntriesFromDictionary: unicodeDict];
      return combinedDict;
    }
  else
    return charDict;

}

- (NSString *) getName: (int) number
{
  NSString *name;
  NSString *numberString;
  
  numberString = [self getNumberString: number];
  if ([unicodeDict objectForKey: numberString])
    name = [[unicodeDict objectForKey: numberString] objectAtIndex: 0];
  else 
    {
      if (number > 0x3400 && number < 0x4DB5)
	name = _(@"<CJK Ideograph Extension A>");
      else if (number > 0x4E00 && number < 0x9FA5)
	name = _(@"<CJK Unified Ideograph>");
      else if (number > 0xAC00 && number < 0xD7A3)
	name = _(@"<Hangul Syllable>");
      else if (number > 0xD800 && number < 0xDB7F)
	name = _(@"<Non Private Use High Surrogate>");
      else if (number > 0xDB80 && number < 0xDBFF)
	name = _(@"<Private Use High Surrogate>");
      else if (number > 0xDC00 && number < 0xDFFF)
	name = _(@"<Low Surrogate>");
      else if (number > 0xE000 && number < 0xF8FF)
	name = _(@"<Private Use>");
//       else if (number > 0x20000 && number < 0x2A6D6)
// 	name = _(@"<CJK Ideograph Extension B>");
//       else if (number > 0xF0000 && number < 0xFFFD)
// 	name = _(@"<Plane 15 Private Use>");
//       else if (number > 0x100000 && number < 0x10FFFD)
// 	name = _(@"<Plane 16 Private Use>");
      else
	name = _(@"<not assigned>");
    }
  return name;
}

- (NSString *) getAlias: (int) number
{
  NSDictionary *d = [self dictionaryForCharacter: number];
  return [[d objectForKey: @"Unicode1.0Name"] retain];
}

- (NSString *) getCategory: (int) number
{
  NSString *cat;

  if (number > 0x3400 && number < 0x4DB5)
    cat = @"Lo";
  else if (number > 0x4E00 && number < 0x9FA5)
    cat = @"Lo";
  else if (number > 0xAC00 && number < 0xD7A3)
    cat = @"Lo";
  else if (number > 0xD800 && number < 0xDB7F)
    cat = @"Cs";
  else if (number > 0xDB80 && number < 0xDBFF)
    cat = @"Cs";
  else if (number > 0xDC00 && number < 0xDFFF)
    cat = @"Cs";
  else if (number > 0xE000 && number < 0xF8FF)
    cat = @"Co";
  else
    cat = nil;
  
  return cat;
}

- (NSString *) getFullCategoryName: (NSString *) abbreviation
{
  return ([categoriesDict objectForKey: abbreviation]);
}

- (NSString *) getFullDecomposition: (NSString *) s
{
  NSArray *disregard;
  NSArray *parts;
  NSMutableString *fullDecomp;
  NSString *name = nil;
  short int i = 0, j = 0;
  int charValue;

  fullDecomp = [[NSMutableString alloc] init];
  if ([s length] > 0)
    {
      parts = [[NSString stringWithUTF8String: [s UTF8String]]
		componentsSeparatedByString: @" "];

      disregard = [[NSArray alloc] initWithObjects: _(@"<fraction>"),
				   _(@"<circle>"), _(@"<compat>"), 
				   _(@"<control>"), nil];
      for (j = 0; j < [parts count]; j++)
	{
	  if ([disregard containsObject: [parts objectAtIndex: j]])
	    i = (i + 1);
	}

      while(i < [parts count])
	{
	  if ([[parts objectAtIndex: i] isEqual: @"<super>"])
	    {
	      [fullDecomp appendString: [parts objectAtIndex: i]];	      
	    }
	  else
	    {
	      [fullDecomp appendString: [NSString stringWithFormat: @"U+%s ",
						  [[parts objectAtIndex: i]
						    cString]]];
	      charValue = strtol([[parts objectAtIndex: i] cString], NULL, 16);
	      name = [[NSString alloc] initWithString: [self getName: charValue]];
	      [fullDecomp appendString: [NSString stringWithFormat: @"%s",
						  [name cString]]];
	    }
	  if ([parts count] > (i + 1))
	    [fullDecomp appendString: [NSString stringWithString: @" + "]];
	  i++;
	}
    }
  return fullDecomp;
}

@end

