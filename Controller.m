// Controller.m - Charmap application delegate implementation
// Copyright (C) 2003,2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CONTROLLER_H
#include "Controller.h"
#endif

@implementation Controller : NSObject
- (void) dealloc
{
  RELEASE(charmapCell);
  RELEASE(charmapMatrix); 
  RELEASE(charmapScroll);
  RELEASE(largeDict);
  RELEASE(cellDict);
  RELEASE(display);
  RELEASE(CJKInspector);
  RELEASE(cjkChar);
  RELEASE(cjkDefinition);
  RELEASE(cjkMenuItem);
  RELEASE(charInspector);
  RELEASE(preferencesPanel);
  RELEASE(mainMenu);

  RELEASE(charInspector);
  RELEASE(cjkInspector);
  RELEASE(preferences);
  RELEASE(unicodeData);
  [super dealloc];
}

- (id) init
{
  preferences = [[Preferences alloc] init];
  charInspector = [[CharInspector alloc] init];
  cjkInspector = [[CJKInspector alloc] init];
  unicodeData = [[UnicodeData alloc] init];
  charInspectorOpen = NO;
  cjkInspectorOpen = NO;

  showCJK = [preferences getCJK];
  if (showCJK == YES)
    [unicodeData enableCJK];

  return self;
}

- (void) awakeFromNib
{
  short int i;
  
  NSMutableParagraphStyle * paragraphStyle = [NSMutableParagraphStyle
					       defaultParagraphStyle];
  [paragraphStyle setAlignment: NSCenterTextAlignment];
  ASSIGN(cellDict, ([NSMutableDictionary dictionaryWithObjectsAndKeys:
					   [NSFont systemFontOfSize: 14],
					 NSFontAttributeName,
					 [NSColor blackColor],
					 NSForegroundColorAttributeName,
					 paragraphStyle,
					 NSParagraphStyleAttributeName,
					 nil]));
  ASSIGN(largeDict, ([NSMutableDictionary dictionaryWithObjectsAndKeys:
					    [NSFont systemFontOfSize: 48],
					  NSFontAttributeName,
					  [NSColor blueColor],
					  NSForegroundColorAttributeName,
					  paragraphStyle,
					  NSParagraphStyleAttributeName,
					  nil]));

  /* Set some main window graphical properties. */
  [blocksBrowser setMinColumnWidth: 250];
  [blocksBrowser setMaxVisibleColumns:1];
  [blocksBrowser setAction: @selector (generateCharmap)];
  [blocksBrowser setTarget: self];
  /* for some reason i dont get this to work from the gorm file: */
  [(NSSplitView *)[blocksBrowser superview] setDelegate:self];
  charmapCell = [[NSTextFieldCell alloc] init];
  [charmapCell setBordered: NO];
  [charmapCell setEditable: NO];
  [charmapCell setSelectable: NO];
  [charmapMatrix setAllowsEmptySelection: NO];
  [charmapMatrix setAction: @selector (updateInspectors:)];
  [charmapMatrix setAllowsEmptySelection: NO];
  [charmapMatrix setAutoscroll: YES];
  [charmapMatrix setBackgroundColor: [NSColor windowBackgroundColor]];
  [charmapMatrix setCellBackgroundColor: [NSColor whiteColor]];
  [charmapMatrix setCellSize: NSMakeSize (30, 35)];
  [charmapMatrix setDoubleAction: @selector (putSelected:)];
  [charmapMatrix setDrawsCellBackground: YES];
  [charmapMatrix setMode: NSRadioModeMatrix];
  [charmapMatrix setPrototype: charmapCell];
  [charmapMatrix setSelectionByRect: YES];
  [charmapMatrix setAutosizesCells:NO];
  for (i = 0; i < 10; i++)
    [charmapMatrix addColumn];
  [copyButton setEnabled: NO];
  [display setFont: [cellDict objectForKey: NSFontAttributeName]];

  /* Set some CJK Inspector graphical properties */
  [cjkChar setAlignment: NSLeftTextAlignment];
  [cjkChar setDrawsBackground: NO];
  [cjkChar setEditable: NO];
  [cjkChar setRichText: YES];
  [cjkChar setSelectable: NO];
  [cjkChar setUsesFontPanel: NO];
  [cjkDefinition setDrawsBackground: NO];
  [cjkDefinition setEditable: NO];
  [cjkDefinition setSelectable: NO];
  [[cjkDefinition textContainer]
    setContainerSize: NSMakeSize(185, 45)];
  [[cjkDefinition textContainer] setWidthTracksTextView: YES];
  [[cjkDefinition textStorage] setAttributedString:
				 [[NSAttributedString alloc]
				   initWithString: nil]];


  [self generateCharmap];
}

- (void) generateCharmap
{
  NSString *tempString = nil;
  NSTextFieldCell *tempCell;
  unichar aChar;
  int i, j, k, block, blockSize, columns;
  unsigned int blockStart, blockEnd;

  /* Remove all rows BUT ONE*/
  for (i = [charmapMatrix numberOfRows]; i >= 1; i--)
    {
      if ([charmapMatrix cellAtRow: i column: 0])
	[charmapMatrix removeRow: i];
    }

  /* whiten the remaining row */
  for (j = [charmapMatrix numberOfColumns]; j>=0; j--)
    [[charmapMatrix cellAtRow:0 column:j] 
      setBackgroundColor: [NSColor whiteColor]];

  /*  Add the necessary rows */
  block = [blocksBrowser selectedRowInColumn: 0];
  if (block == -1)
    block = 0;
  blockSize = [unicodeData getBlockSize: block];
  blockStart = [unicodeData getBlockStart: block];
  blockEnd = [unicodeData getBlockEnd: block];
  columns = [charmapMatrix numberOfColumns];
  for (i = 0; i < ((blockSize-1) / columns); i++)
    [charmapMatrix insertRow:0];

  for (k=blockStart; k<=blockEnd; k++)
    { 
          i=(k-blockStart)/columns;j=(k-blockStart)%columns;
	  aChar = k;
	  tempString = [[NSString alloc] initWithCharacters: &aChar length: 1];
	  tempCell = [charmapMatrix cellAtRow: i column: j];
	  [charmapCell setAllowsEditingTextAttributes: NO];
	  [tempCell setTag: k];
	  [tempCell setAlignment: NSCenterTextAlignment];
	  [tempCell setAttributedStringValue: [[NSAttributedString alloc]
						initWithString: tempString
						attributes: cellDict]];
	  [tempCell sendActionOn: 0];
	  if ( [[unicodeData getName: k] isEqual: @"<not assigned>"] == NO
	       && (k < 55296 || k > 57339))
	    {
	      [tempCell setEnabled: YES];
	      [tempCell setAction: @selector(updateInspectors:)];
	    }
	  else // Disable positions which are not assigned.
	    {
	      [tempCell setBackgroundColor: [NSColor grayColor]]; 
	      [tempCell setDrawsBackground: YES];
	      [tempCell setEnabled: YES];
	      [tempCell setAttributedStringValue: nil];
	    }
	  [tempCell sendActionOn: NSLeftMouseDownMask];
	  RELEASE(tempString);
	}

  for (j++;j<columns;j++)
    {
      tempCell=[charmapMatrix  cellAtRow: i column: j];
      [tempCell setEnabled:NO];
      [tempCell setBackgroundColor: [charmapMatrix backgroundColor]];
      [tempCell setDrawsBackground: YES];
      [tempCell setAttributedStringValue: nil];
    }
  /* Set next view on first and last cells */
  /* for (i = [charmapMatrix numberOfRows]; i >= 0; i--)
     {
     [[charmapMatrix cellAtRow: i column: 9]
     setNextKeyView: [charmapMatrix cellAtRow: (i+1) column: 0]];
	} */

  /* Finish setting display. */
  [charmapMatrix sizeToCells];
  [charmapMatrix scrollCellToVisibleAtRow: 0 column: 0];
  [charmapMatrix selectCellAtRow: 0 column: 0];
  [charmapMatrix setNeedsDisplay: YES];
  [charmapMatrix setAction: @selector (updateInspectors:)];
  if (charInspectorOpen == YES)
    [self updateInspectors: self];
}

- (void) controlTextDidChange: (NSNotification *) aNotification
{
  if ([[display stringValue] length])
    [copyButton setEnabled: YES];
  else
    [copyButton setEnabled: NO];
}

- (void) putSelected: (id) sender
{
  NSMutableString *selectedString;
  int charTag;

  charTag = [[charmapMatrix selectedCell] tag];
  selectedString = [[NSMutableString alloc] init];
  [selectedString appendString: [display stringValue]];
  [selectedString appendString: [[charmapMatrix selectedCell] stringValue]];
  [display setStringValue: selectedString];
  [self controlTextDidChange: (NSNotification *)
	NSControlTextDidChangeNotification];
}

- (void) copySelected: (id) sender
{
  NSPasteboard *pb = [NSPasteboard generalPasteboard];

  [display selectText: self];
  [pb declareTypes: [NSArray arrayWithObject: NSStringPboardType]
      owner: self];
  if ([[display stringValue] length] != 0)
    {
      [pb setString: [display stringValue] forType: NSStringPboardType];
      NSLog (_(@"Text copied."));
    }
  else
    NSLog (_(@"No text selected."));
}

- (void) updateInspectors: (id) sender
{
  NSDictionary *charDict;
  unichar charTag;

  charTag = [[charmapMatrix selectedCell] tag];
  
  charDict = [unicodeData dictionaryForCharacter: charTag];
  
  if (charInspectorOpen == YES)
      [charInspector updateWithPosition: charTag
		     Dictionary: charDict];
  if (showCJK && cjkInspectorOpen == YES)
    [cjkInspector updateWithPosition: charTag
		   Dictionary: charDict];
}

- (void) showPreferences: (id) sender
{
  [NSBundle loadNibNamed: @"Preferences" owner: preferences];
}

- (void) showCharInspector: (id) sender
{
  [NSBundle loadNibNamed: @"CharInspector" owner: charInspector];
  charInspectorOpen = YES;
  [self updateInspectors: self];
}

- (void) showCJKInspector: (id) sender
{
  [NSBundle loadNibNamed: @"CJKInspector" owner: cjkInspector];
  cjkInspectorOpen = YES; 
  [self updateInspectors: self];
}

- (void) changeFont: (id) fontManager
{
  NSFont *newCellFont;
  NSFont *newLargeFont;
  unichar selectedTag;

  selectedTag = [[charmapMatrix selectedCell] tag];

  /* Create new fonts */
  newCellFont = [fontManager convertFont: [cellDict objectForKey:
						 NSFontAttributeName]];
  [cellDict setObject: newCellFont forKey: NSFontAttributeName];

  newLargeFont = [fontManager convertFont: [largeDict objectForKey: 
						   NSFontAttributeName]];

  [largeDict setObject: [fontManager convertFont: newLargeFont toSize: 48] 
	     forKey: NSFontAttributeName];
  
  /* Update display */
  [display setFont: newCellFont];
  [display setNeedsDisplay: YES];
  [self generateCharmap];
  [charmapMatrix selectCellWithTag: selectedTag];

  [charInspector setNewFont: newLargeFont
		 Dictionary: largeDict];
  [self updateInspectors: self];
}

- (void) windowWillClose: (NSNotification *) theNotification
{
}

- (void) splitViewDidResizeSubviews:(NSNotification *) theNotification
{
  [self adjustCharmapMatrixWidth];
}

- (void) adjustCharmapMatrixWidth
{
  float newWidth = [[charmapMatrix superview] frame].size.width;
  //[charmapMatrix sizeToCells];
  float contentWidth = [charmapMatrix frame].size.width;
  float difference = newWidth - contentWidth;
  float columnWidth = [charmapMatrix cellSize].width
    +[charmapMatrix intercellSpacing].width;
  
  if (difference < 0) 
    for (;difference<0 && [charmapMatrix numberOfColumns]>1;
	 difference+=columnWidth)
      [charmapMatrix removeColumn:0];
  else if (difference > columnWidth)
    for (;difference>columnWidth;difference-=columnWidth)
      [charmapMatrix addColumn];
  else return;

  [self generateCharmap];
}


- (void) toggleCJK: (NSNumber*) cjkState
{
  if ([cjkState boolValue] == YES)
    {
      showCJK = YES;
      NSLog(@"Controller is calling UnicodeData to enable CJK...");
      [unicodeData enableCJK];
    }
  if ([cjkState boolValue] == NO)
    {
      showCJK = NO;
      NSLog(@"Controller is calling UnicodeData to disable CJK...");
      [unicodeData disableCJK];
    }
}

- (BOOL) validateMenuItem: (id <NSMenuItem>) anItem
{
  if ([[anItem title] isEqualToString:@"CJK Inspector"]
      && showCJK == NO)
    return NO;
  else
    return YES;
}

- (void) browser: (NSBrowser *)sender
createRowsForColumn: (int)column
	inMatrix: (NSMatrix *)matrix
{
  short int i;
  int count;
  NSBrowserCell *cell;
  NSString *category;
  blockNames = [unicodeData getBlockNames];
  count = [blockNames count];
  if (count)
    {
      [matrix addColumn];
      for (i = 0; i < count; i++)
	{
	  if (i > 0)
	    [matrix addRow];
	  category = [blockNames objectAtIndex: i];
	  cell = [matrix cellAtRow: i
			 column: 0];
	  [cell setStringValue: category];
	  [cell setLeaf: YES];
	}
    }
}
@end
