// UnicodeData.h - Charmap application Unicode data header
// Copyright (C) 2003,2004 Christopher Culver <crculver@users.sourceforge.net>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef _CHARMAP_H_
#include <Charmap.h>
#endif

#ifndef _UNICODEDATA_H_
#define _UNICODEDATA_H_

@interface UnicodeData : NSObject
{
  NSArray *blockNames;
  NSDictionary *categoriesDict;
  NSDictionary *unicodeDict;
  NSDictionary *unihanDict;

  BOOL cjk;
  unsigned int blockOffsets[126];
}
- (void) enableCJK;
- (void) disableCJK;
- (NSArray *) getBlockNames;
- (int) getBlockSize: (int) unicodeBlock;
- (long) getBlockStart: (int) unicodeBlock;
- (long) getBlockEnd: (int) unicodeBlock;
- (NSDictionary *) dictionaryForCharacter: (int) number;
- (NSString *) getName: (int) number;
- (NSString *) getAlias: (int) number;
- (NSString *) getCategory: (int) number;
- (NSString *) getFullCategoryName: (NSString *) abbreviation;
- (NSString *) getFullDecomposition: (NSString *) s;
@end

#endif /* _UNICODEDATA_H_ */
